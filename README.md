# GOSCIU - painting robot arm
## Siumulation

### Clone repo
Clone this repo wherever you want, either by http(this is preferred if you don't
know what you're exactly doing or ssh.
##### HTTP
`git clone https://gitlab.com/lukaszfl/ws_gosciu.git`
##### SSH
`git clone git@gitlab.com:lukaszfl/ws_gosciu.git`


### HOW TO USE
First you need to build it as catkin packages. To do that navigate to 
***/gosciu_ws/** and run `catkin_make`. Then , if the build was succesful, you
need to source your command
line by using `source devel/setup.bash`.

Now you should be able to use provided launchfiles. Try
`roslaunch gosciu_gazebo gosciu.launch` to launch model in gazebo or
`roslaunch gosciu_gazebo rviz.launch` to run robot only in ROS with
visualization in rViz and possibility to change robot's joints. 

If robot is not visible change *Fixed frame* to **link_base** and click *Add* in
the bottom left and add by display type **RobotModel**.

