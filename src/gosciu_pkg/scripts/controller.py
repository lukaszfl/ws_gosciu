#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

import roslib
roslib.load_manifest('gosciu_pkg')
import actionlib

# import custom actions
from gosciu_pkg.msg import move_cartesian_globalAction
from gosciu_pkg.msg import move_cartesian_relativeAction
from gosciu_pkg.msg import move_joints_relativeAction
from gosciu_pkg.msg import move_cartesian_pathAction
from gosciu_pkg.msg import move_joints_globalAction

def check_tolerance(goal, actual, tolerance):
  """
  Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
  @param: goal       A list of floats, a Pose or a PoseStamped
  @param: actual     A list of floats, a Pose or a PoseStamped
  @param: tolerance  A float
  @returns: bool
  """
  all_equal = True
  if type(goal) is list:
    for index in range(len(goal)):
      if abs(actual[index] - goal[index]) > tolerance:
        return False

  elif type(goal) is geometry_msgs.msg.PoseStamped:
    return check_tolerance(goal.pose, actual.pose, tolerance)

  elif type(goal) is geometry_msgs.msg.Pose:
    return check_tolerance(pose_to_list(goal), pose_to_list(actual), tolerance)

  return True

class Controller(object):
  def __init__(self):
    super(Controller, self).__init__()

    # Load args
    name = rospy.get_param('group_name', 'gosciu_arm')
    # no need to have different tolerances for every joint or axis because of the same drives
    self.joint_tolerance = float(rospy.get_param('joint_tolerance', '0.0174533'))  # in radians
    self.cartesian_tolerance = float(rospy.get_param('cartesian_tolerance', '0.005'))  # in meters
    # robot_description = rospy.get_param('robot_description', 'gosciu_description')

    ## First initialize `moveit_commander`_ and a `rospy`_ node:
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('controller_node', anonymous=False)
    ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
    ## the robot:
    robot = moveit_commander.RobotCommander()

    ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
    ## to the world surrounding the robot:
    scene = moveit_commander.PlanningSceneInterface()


    group_name = name
    group = moveit_commander.MoveGroupCommander(group_name)

    ## We create a `DisplayTrajectory`_ publisher which is used later to publish
    ## trajectories for RViz to visualize:
    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)

    # We can get the name of the reference frame for this robot:
    planning_frame = group.get_planning_frame()
    print "============ Reference frame: %s" % planning_frame

    # We can also print the name of the end-effector link for this group:
    eef_link = group.get_end_effector_link()
    print "============ End effector link: %s" % eef_link

    # We can get a list of all the groups in the robot:
    group_names = robot.get_group_names()
    print "============ Robot Groups:", robot.get_group_names()

    # Sometimes for debugging it is useful to print the entire state of the
    # robot:
    print "============ Printing robot state"
    print robot.get_current_state()
    print ""

    # Misc variables
    self.box_name = ''
    self.robot = robot
    self.scene = scene
    self.group = group
    self.display_trajectory_publisher = display_trajectory_publisher
    self.planning_frame = planning_frame
    self.eef_link = eef_link
    self.group_names = group_names

    # Create action callbacks
    self.cartesian_global_server = actionlib.SimpleActionServer('move_cartesian_global', move_cartesian_globalAction, self.go_to_pose_goal_global, False)
    self.cartesian_relative_server = actionlib.SimpleActionServer('move_cartesian_relative', move_cartesian_relativeAction, self.go_to_pose_goal_relative, False)
    self.cartesian_path_server = actionlib.SimpleActionServer('move_cartesian_path', move_cartesian_pathAction, self.go_to_cartesian_path, False)
    self.joints_global_server = actionlib.SimpleActionServer('move_joints_global', move_joints_globalAction, self.go_to_joint_state_global, False)
    self.joints_relative_server = actionlib.SimpleActionServer('move_joints_relative', move_joints_relativeAction, self.go_to_joint_state_relative, False)

    # Activate the callbacks
    self.cartesian_global_server.start()
    self.cartesian_relative_server.start()
    self.cartesian_path_server.start()
    self.joints_global_server.start()
    self.joints_relative_server.start()


    print 'Controller init complete!'
    rospy.spin()


  def go_to_pose_goal_global(self, cartesian_goal_global):
    """
    This method will plan and execute move to given global position
    @param cartesian_goal_global - message given in callback of type move_cartesian_globalActionGoal
    """
    print cartesian_goal_global
    cartesian_goal_global = list(cartesian_goal_global.goal_global_position)
    ## We can plan a motion for this group to a desired pose for the
    ## end-effector:
    pose_goal = geometry_msgs.msg.Pose()
    try:
      pose_goal.orientation.w = cartesian_goal_global[0]
      pose_goal.position.x = cartesian_goal_global[1]
      pose_goal.position.y = cartesian_goal_global[2]
      pose_goal.position.z = cartesian_goal_global[3]
    except:
      print 'data format is wrong'
      return False

    self.group.set_pose_target(pose_goal)

    ## Now, we call the planner to compute the plan and execute it.
    plan = self.group.go(wait=True)
    # Calling `stop()` ensures that there is no residual movement
    self.group.stop()
    # It is always good to clear your targets after planning with poses.
    # Note: there is no equivalent function for clear_joint_value_targets()
    self.group.clear_pose_targets()

    # For testing:
    # Note that since this section of code will not be included in the tutorials
    # we use the class variable rather than the copied state variable
    current_pose = self.group.get_current_pose().pose
    if check_tolerance(pose_goal, current_pose, self.cartesian_tolerance):
      self.cartesian_global_server.set_succeeded()
      return True
    else:
      self.cartesian_global_server.set_aborted()
      return False

  def go_to_pose_goal_relative(self, cartesian_goal_relative):
    """
    This method will plan and execute move to given relative position
    @param cartesian_goal_relative - message given in callback of type move_cartesian_relativeActionGoal
    """
    print cartesian_goal_relative
    cartesian_goal_relative = list(cartesian_goal_relative.goal_relative_position)
    ## We can plan a motion for this group to a desired pose for the
    ## end-effector:
    pose_goal = self.group.get_current_pose().pose
    try:
      pose_goal.orientation.w = pose_goal.orientation.w + cartesian_goal_relative[0]
      pose_goal.position.x = pose_goal.position.x + cartesian_goal_relative[1]
      pose_goal.position.y = pose_goal.position.y + cartesian_goal_relative[2]
      pose_goal.position.z = pose_goal.position.z + cartesian_goal_relative[3]
    except:
      print 'data format is wrong'
      return False

    self.group.set_pose_target(pose_goal)

    ## Now, we call the planner to compute the plan and execute it.
    plan = self.group.go(wait=True)
    # Calling `stop()` ensures that there is no residual movement
    self.group.stop()
    # It is always good to clear your targets after planning with poses.
    # Note: there is no equivalent function for clear_joint_value_targets()
    self.group.clear_pose_targets()

    # For testing:
    # Note that since this section of code will not be included in the tutorials
    # we use the class variable rather than the copied state variable
    current_pose = self.group.get_current_pose().pose
    if check_tolerance(pose_goal, current_pose, self.cartesian_tolerance):
      self.cartesian_relative_server.set_succeeded()
      return True
    else:
      self.cartesian_relative_server.set_aborted()
      return False


  # TODO
  def go_to_cartesian_path(self, cartesian_goal_path):
    ## You can plan a Cartesian path directly by specifying a list of waypoints
    ## for the end-effector to go through:
    ##
    # waypoints = []

    # wpose = self.group.get_current_pose().pose
    # wpose.position.z -= scale * 0.1  # First move up (z)
    # wpose.position.y += scale * 0.2  # and sideways (y)
    # waypoints.append(copy.deepcopy(wpose))

    # wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
    # waypoints.append(copy.deepcopy(wpose))

    # wpose.position.y -= scale * 0.1  # Third move sideways (y)
    # waypoints.append(copy.deepcopy(wpose))

    # # We want the Cartesian path to be interpolated at a resolution of 1 cm
    # # which is why we will specify 0.01 as the eef_step in Cartesian
    # # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
    # (plan, fraction) = self.group.compute_cartesian_path(
    #                                    waypoints,   # waypoints to follow
    #                                    0.01,        # eef_step
    #                                    0.0)         # jump_threshold

    # # Move robot
    # self.group.execute(plan, wait=True)

    self.cartesian_path_server.set_succeeded()
    return True


  def go_to_joint_state_global(self, joint_goal_global):
    """
    This method will plan and execute move to given global joint states
    @param joint_goal_global - message given in callback of type move_joints_globalActionGoal
    """
    print joint_goal_global
    joint_goal_global = list(joint_goal_global.goal_global_joint_states)
    # We can get the joint values from the group and adjust some of the values:
    joint_goal = self.group.get_current_joint_values()
    # run this loop to check if number of joints is ok
    if len(joint_goal) != len(joint_goal_global):
      print 'number of joints do not match the one of a robot'
      self.joints_relative_server.set_aborted()
      return False

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    self.group.go(joint_goal_global, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    self.group.stop()
    # For testing:
    # Note that since this section of code will not be included in the tutorials
    # we use the class variable rather than the copied state variable
    current_joints = self.group.get_current_joint_values()
    if check_tolerance(joint_goal, current_joints, self.joint_tolerance):
      self.joints_global_server.set_succeeded()
      return True
    else:
      self.joints_global_server.set_aborted()
      return False

  def go_to_joint_state_relative(self, joint_goal_relative):
    """
    This method will plan and execute move to given relative joint states
    @param joint_goal_relative - message given in callback of type move_joints_relativeActionGoal
    """
    print joint_goal_relative
    joint_goal_relative = list(joint_goal_relative.goal_relative_joint_states)
    # We can get the joint values from the group and adjust some of the values:
    joint_goal = self.group.get_current_joint_values()

    for i in range(0, len(joint_goal)):
      try:
        joint_goal[i] = joint_goal[i] + joint_goal_relative[i]
      except:
        print 'no such joint, check number of joints given'
        self.joints_relative_server.set_aborted()
        return False

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    self.group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    self.group.stop()

    # For testing:
    # Note that since this section of code will not be included in the tutorials
    # we use the class variable rather than the copied state variable
    current_joints = self.group.get_current_joint_values()
    if check_tolerance(joint_goal, current_joints, self.joint_tolerance):
      self.joints_relative_server.set_succeeded()
      return True
    else:
      self.joints_relative_server.set_aborted()
      return False



  def display_trajectory(self, plan):

    display_trajectory_publisher = self.display_trajectory_publisher

    ## Displaying a Trajectory
    ## ^^^^^^^^^^^^^^^^^^^^^^^
    ## You can ask RViz to visualize a plan (aka trajectory) for you. But the
    ## group.plan() method does this automatically so this is not that useful
    ## here (it just displays the same trajectory again):
    ##
    ## A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
    ## We populate the trajectory_start with our current robot state to copy over
    ## any AttachedCollisionObjects and add our plan to the trajectory.
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = self.robot.get_current_state()
    display_trajectory.trajectory.append(plan)
    # Publish
    display_trajectory_publisher.publish(display_trajectory)


  def execute_plan(self, plan):
    ## Use execute if you would like the robot to follow
    ## the plan that has already been computed:
    self.group.execute(plan, wait=True)

    ## **Note:** The robot's current joint state must be within some tolerance of the
    ## first waypoint in the `RobotTrajectory`_ or ``execute()`` will fail


  def wait_for_state_update(self, box_is_known=False, box_is_attached=False, timeout=4):
    ## Ensuring Collision Updates Are Receieved
    ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ## If the Python node dies before publishing a collision object update message, the message
    ## could get lost and the box will not appear. To ensure that the updates are
    ## made, we wait until we see the changes reflected in the
    ## ``get_known_object_names()`` and ``get_known_object_names()`` lists.
    ## For the purpose of this tutorial, we call this function after adding,
    ## removing, attaching or detaching an object in the planning scene. We then wait
    ## until the updates have been made or ``timeout`` seconds have passed
    start = rospy.get_time()
    seconds = rospy.get_time()
    while (seconds - start < timeout) and not rospy.is_shutdown():
      # Test if the box is in attached objects
      attached_objects = self.scene.get_attached_objects([self.box_name])
      is_attached = len(attached_objects.keys()) > 0

      # Test if the box is in the scene.
      # Note that attaching the box will remove it from known_objects
      is_known = self.box_name in self.scene.get_known_object_names()

      # Test if we are in the expected state
      if (box_is_attached == is_attached) and (box_is_known == is_known):
        return True

      # Sleep so that we give other threads time on the processor
      rospy.sleep(0.1)
      seconds = rospy.get_time()

    # If we exited the while loop without returning then we timed out
    return False

  # TODO
  def add_box(self, timeout=4):
    pass
    ## Adding Objects to the Planning Scene
    ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ## First, we will create a box in the planning scene at the location of the left finger:
    # box_pose = geometry_msgs.msg.PoseStamped()
    # box_pose.header.frame_id = "panda_leftfinger"
    # box_pose.pose.orientation.w = 1.0
    # self.box_name = "box"
    # self.scene.add_box(self.box_name, box_pose, size=(0.1, 0.1, 0.1))

    # return self.wait_for_state_update(box_is_known=True, timeout=timeout)

  # TODO
  def attach_box(self, timeout=4):
    pass
    # touch_links = self.robot.get_link_names(group=self.eef_link)
    # self.scene.attach_box(self.eef_link, self.box_name, touch_links=touch_links)

    # # We wait for the planning scene to update.
    # return self.wait_for_state_update(box_is_attached=True, box_is_known=False, timeout=timeout)

  # TODO
  def detach_box(self, timeout=4):
    pass
    ## Detaching Objects from the Robot
    ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ## We can also detach and remove the object from the planning scene:
    # self.scene.remove_attached_object(self.eef_link, name=box_name)

    # # We wait for the planning scene to update.
    # return self.wait_for_state_update(box_is_known=True, box_is_attached=False, timeout=timeout)

  # TODO
  def remove_box(self, timeout=4):
    pass
    ## **Note:** The object must be detached before we can remove it from the world
    # try:
    #   self.detach_box()
    # except:
    #   pass
    # ## Removing Objects from the Planning Scene
    # ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    # ## We can remove the box from the world.
    # self.scene.remove_world_object(self.box_name)

    # # We wait for the planning scene to update.
    # return self.wait_for_state_update(box_is_attached=False, box_is_known=False, timeout=timeout)


def main():
  try:
    controller = Controller()

  except rospy.ROSInterruptException:
    return
  except KeyboardInterrupt:
    return

if __name__ == '__main__':
  main()
