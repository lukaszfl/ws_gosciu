#!/usr/bin/env python

import sys
import copy
import rospy
from math import pi
from std_msgs.msg import String

import roslib
roslib.load_manifest('gosciu_pkg')
import actionlib

#import custom actions
from gosciu_pkg.msg import move_cartesian_globalAction, move_cartesian_globalGoal
from gosciu_pkg.msg import move_cartesian_relativeAction, move_cartesian_relativeGoal
from gosciu_pkg.msg import move_joints_relativeAction, move_joints_relativeGoal
from gosciu_pkg.msg import move_cartesian_pathAction, move_cartesian_pathGoal
from gosciu_pkg.msg import move_joints_globalAction, move_joints_globalGoal


class SimpleControlClient(object):
    def __init__(self):
        super(SimpleControlClient, self).__init__()

        # Initialize node
        rospy.init_node('simple_control_client_node', anonymous=False)

        self.cartesian_global_client = actionlib.SimpleActionClient('move_cartesian_global', move_cartesian_globalAction)
        self.cartesian_relative_client = actionlib.SimpleActionClient('move_cartesian_relative', move_cartesian_relativeAction)
        self.cartesian_path_client = actionlib.SimpleActionClient('move_cartesian_path', move_cartesian_pathAction)
        self.joints_global_client = actionlib.SimpleActionClient('move_joints_global', move_joints_globalAction)
        self.joints_relative_client = actionlib.SimpleActionClient('move_joints_relative', move_joints_relativeAction)

        self.cartesian_global_client.wait_for_server()
        self.cartesian_relative_client.wait_for_server()
        self.cartesian_path_client.wait_for_server()
        self.joints_global_client.wait_for_server()
        self.joints_relative_client.wait_for_server()

        print 'Simple Control Cient init complete!!'
        # rospy.spin()


    def move_cartesian_global(self, global_goal=[0.0, 0.0, 0.0, 0.0]):
        """
        This is a method to send action request to move robot's effector to cartesian global coordinates.
        @param global_goal=[w, x, y, z]  global orientation and cartesian coordinates
        """
        # Create a valid message as the .msg add header to the action message
        msg = move_cartesian_globalGoal()
        # Fill the message with given goal
        msg.goal_global_position = global_goal

        # Send message
        self.cartesian_global_client.send_goal(msg)
        # Wait for result
        self.cartesian_global_client.wait_for_result(rospy.Duration.from_sec(10.0))


    def move_cartesian_relative(self, relative_goal=[0.0, 0.0, 0.0, 0.0]):
        """
        This is a method to send action request to move robot's effector to cartesian relative coordinates.
        @param goal=[w, x, y, z]  relative orientation and cartesian coordinates
        """
        # Create a valid message as the .msg add header to the action message
        msg = move_cartesian_relativeGoal()
        # Fill the message with given goal
        msg.goal_relative_position = relative_goal

        # Send message
        self.cartesian_relative_client.send_goal(msg)
        # Wait for result
        self.cartesian_relative_client.wait_for_result(rospy.Duration.from_sec(10.0))


    def move_cartesian_path(self, path_goal=[0.0, 0.0, 0.0, 0.0]):
        """
        This is a method to send action request to move robot's effector to cartesian global coordinates in a given sequence.
        @param global_goal=[w0, x0, y0, z0, w1, x1, y1, z1, ...]  sequence of global orientation and cartesian coordinates
        """
        # Create a valid message as the .msg add header to the action message
        msg = move_cartesian_pathGoal()
        # Fill the message with given goal
        msg.goal_cartesian_path = path_goal

        # Send message
        self.cartesian_path_client.send_goal(msg)
        # Wait for result
        self.cartesian_path_client.wait_for_result(rospy.Duration.from_sec(20.0))


    def move_joints_global(self, global_goal=[0.0, 0.0, 0.0, 0.0]):
        """
        This is a method to send action request to move robot's joints to the requested global ones.
        @param global_goal=[j0, j1, j2, j3]  global joint states
        """
        # Create a valid message as the .msg add header to the action message
        msg = move_joints_globalGoal()
        # Fill the message with given goal
        msg.goal_global_joint_states = global_goal

        # Send message
        self.joints_global_client.send_goal(msg)
        # Wait for result
        self.joints_global_client.wait_for_result(rospy.Duration.from_sec(10.0))


    def move_joints_relative(self, relative_goal=[0.0, 0.0, 0.0, 0.0]):
        """
        This is a method to send action request to move robot's joints to the requested relative ones.
        @param global_goal=[j0, j1, j2, j3]  relative joint states
        """
        # Create a valid message as the .msg add header to the action message
        msg = move_joints_relativeGoal()
        # Fill the message with given goal
        msg.goal_relative_joint_states = relative_goal

        # Send message
        self.joints_relative_client.send_goal(msg)
        # Wait for result
        self.joints_relative_client.wait_for_result(rospy.Duration.from_sec(10.0))



def main():
    try:
        simple_control_client = SimpleControlClient()

    except rospy.ROSInterruptException:
        return


if __name__ == '__main__':
  main()
